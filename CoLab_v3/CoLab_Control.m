classdef CoLab_Control<handle

    properties (Access=public)
        %% Classes Instances
        Audio=CoLab_Audio;
        Tobii=CoLab_Tobii3;
        Motive=[]; % Added in runtime
        %% Global Variables
        groupName=''; % group id, e.g. participants initials
        cond=''; % condition, i.e. noisy or quiet
        gain=0; % gain multiplier, to be sent to audio player
        gainQuiet=0.05; gainNoisy=1.5; % defined gain multiplier for each condition
        item=''; % list of items for task
        %% States
        recFlag=false; % recording bool for the whole apparatus
        connectFlag=false; % connected bool for Motive (Optitrack)
        calibrateFlag=false; % calibrated bool fro Tobii glasses
    end

    methods
        function CreateUI(obj)

            %% LIBRARIES and DEVICES
            %             unzip('NatNet_SDK_4.0.zip');
            %             addpath(genpath('NatNetSDK'));
            obj.Audio.CreateDevices();

            %% PANELS
            % Main Window (Figure)
            fig = uifigure('Position',[300 100 1200 900]);
            fig.Name='CoLab Controller';
            uiTitle=uilabel(fig,'Text','CoLab Controller (BETA)',...
                'Position',[400 800 400 100],'FontSize',28);

            % Output
            outPanel=uipanel(fig,'BackgroundColor','white','Position',...
                [350 70 750 400],'Title','Output','FontSize',20);

            compRecPanel=uipanel(outPanel,'BackgroundColor','white',...
                'Position',[30 30 680 250],'Title','Completed Recordings','FontSize',20);
            compRecLabel=uilabel(compRecPanel,'Position',[10 10 800 200],...
                'FontSize',18);
            compRecLabel.Text='';

            tLeftPanel=uipanel(outPanel,'BackgroundColor','white',...
                'Position',[30 290 680 70],'Title','Rec. Time Left (sec)','FontSize',20);
            tLeftLabel=uilabel(tLeftPanel,'Position',[10 10 800 25],...
                'FontSize',18,'Text','0');
            tLeft=0;

            % Experiment Manager
            expPanel=uipanel(fig,'Title','Experiment Manager','FontSize',20,...
                'BackgroundColor','white','Position',[100 630 1000 200]);

            groupNameTitle=uilabel(expPanel,'Text','Group Name:',...
                'Position',[50 100 300 50],'FontSize',18);
            groupNameField=uieditfield(expPanel,'Position',[50 60 300 50],'FontSize',18,...
                'ValueChangedFcn',@(groupNameField,event) UpdateGroupName...
                (groupNameField.Value));

            condGroup = uibuttongroup(expPanel,'Position',[400 30 100 100],...
                'SelectionChangedFcn',@UpdateCondSelection);
            btnNone=uiradiobutton(condGroup,'Position',[10 70 100 20],...
                'Text','None','FontSize',18);
            btnQuiet = uiradiobutton(condGroup,'Position',[10 40 100 20],...
                'Text','Quiet','FontSize',18);
            btnNoisy = uiradiobutton(condGroup,'Position',[10 10 100 20],...
                'Text','Noisy','FontSize',18);

            itemListTitle=uilabel(expPanel,'Text','Item List:',...
                'Position',[550 100 300 50],'FontSize',18);
            itemList=uidropdown(expPanel,'Position',[550 60 100 50],'Items',...
                {'None','CPH1','CPH2','MOV1','MOV2','EUR1','EUR2','LIN1','LIN2','SHP1','SHP2','TEC1','TEC2'},'FontSize',18,'ValueChangedFcn',...
                @(itemList,event) UpdateItem(itemList.Value));

            lengthTitle=uilabel(expPanel,'Text','Rec. Length (sec):',...
                'Position',[700 60 250 50],'FontSize',18);
            recLenField=uieditfield(expPanel,'numeric','Value',600,'Position',[850 60 100 50],...
                'FontSize',18,'ValueChangedFcn',@(recLenField,event)...
                UpdatetLeft(recLenField,tLeftLabel));

            % Devices
            devicePanel=uipanel(fig,'Title','Device Manager','FontSize',20,...
                'BackgroundColor','white','Position',[100 480 1000 140]);
            btnCalibrate=uibutton(devicePanel,'ButtonPushedFcn',@CalibrateTobii,'Position',...
                [500 30 150 50],'Text','Calibrate Tobii','FontSize',18);
            resp1=uilabel(devicePanel,'Position',[700 0 200 50],'FontSize',18,'Text','TG-11: Not Calibrated');
            resp2=uilabel(devicePanel,'Position',[700 30 200 50],'FontSize',18,'Text','TG-21: Not Calibrated');
            resp3=uilabel(devicePanel,'Position',[700 60 200 50],'FontSize',18,'Text','TG-81: Not Calibrated');

            btnConnectmotive=uibutton(devicePanel,'ButtonPushedFcn',@ConnectMotive,'Position',...
                [50 30 150 50],'Text','Connect Motive','FontSize',18);

            resp4=uilabel(devicePanel,'Position',[250 30 200 50],'FontSize',18,'Text','Motive not connected');

            % Recording
            recPanel=uipanel(fig,'Title','Recording','FontSize',20,...
                'BackgroundColor','white','Position',[100 70 240 400]);
            btnStart=uibutton(recPanel,'ButtonPushedFcn',@StartRec,'Position',...
                [50 270 150 50],'Text','START','FontSize',18);
            btnStop=uibutton(recPanel,'ButtonPushedFcn',@StopRec,'Position',...
                [50 180 150 50],'Text','STOP','FontSize',18,...
                'Enable','off');
            btnClose = uibutton(recPanel,'ButtonPushedFcn',@CloseUI,'Position',...
                [50 40 150 50],'Text','Close','FontSize',18);

            %% CALLBACK FUNCTIONS
            function CloseUI(src,event)
                obj.Audio.ReleaseDevices();
                closereq(); % close UI
            end

            function ConnectMotive(src,event)
                if obj.connectFlag==false
                    Motive=natnet(); % instantiate class
                    disp('Connecting to Motive...');
                    Motive.connect; % connect function
                    disp('Connection with Motive completed');
                    obj.connectFlag=true;
                    resp4.Text='Motive connected ✓'; resp4.FontColor=[0.4660 0.6740 0.1880];
                    btnConnectmotive.Enable='off';
                end
            end

            function CalibrateTobii(src,event)
                calibState=obj.Tobii.SendAction('Calibrate');

                %check calibration
                if all(calibState)
                    obj.calibrateFlag=true;
                else
                    obj.calibrateFlag=false;
                end

                UiCalibration(calibState);
            end

            function StartRec(src,event)

                %% Warning messages
                                if obj.cond==""||obj.item==""||recLenField.Value==0
                                    uialert(fig,'Not all the experiment conditions have been defined!',...
                                        'OBS')
                                    return
                                end
                
                                if obj.calibrateFlag==false
                                    selection=uiconfirm(fig,'Tobii not calibrate! Press OK to start recording without calibrating.','OBS', ...
                                        'DefaultOption',2,'Icon','warning');
                                    if strcmp(selection,'Cancel')
                                        return
                                    end
                                    %                                     uialert(fig,'Tobii not calibrate!','OBS')
                                    %                                     return
                                end
                %
                %                 if obj.connectFlag==false
                %                     uialert(fig,'Motive not connected!','OBS')
                %                     return
                %                 end

                %% Recording
                obj.recFlag=true;
                UiEnable(); % disable buttons and panels


                %start tobii
                try
                   startTobiiState=obj.Tobii.SendAction('StartRec');
                catch
                    uialert(fig,'Tobii not connected!','OBS')
                    obj.recFlag=false;
                    UiEnable(); % disable buttons and panels
                    return
                end

                if ~all(startTobiiState)
                    uialert(fig,'Tobii not connected!','OBS')
                    obj.recFlag=false;
                    UiEnable(); % disable buttons and panels
                    return
                end

                halfTime=recLenField.Value/2; % rec length / 2
                reminder=false; % flag for half-time reminder
                tLeft=recLenField.Value; % time left


                %preallocate memory for audio data
                obj.Audio.audioData=nan(obj.Audio.Fs*recLenField.Value,9);

                %calibrate for temporal alignment
%                 disp('Calibration in 1 second')
%                 pause(1)
%                 tic
%                 for source = [1,6,11]
%                     obj.Audio.PlaybackAndRecCalibration(source);
%                     pause(1)
%                     toc
%                 end
%                 
%                 disp('Recording starts in 3 seconds');
%                 pause(3)

                startTime=tic;
                while  obj.recFlag==true && tLeft>=0
                    tLeft=recLenField.Value-toc(startTime); % update rec time left
                    tLeftLabel.Text=string(tLeft); % write time left
                    if(tLeft<halfTime && reminder==false)
                        uialert(fig,'Half time!',...
                            'OBS','Icon','info');
                        reminder=true;
                    end
                    obj.Audio.PlaybackAndRec();
                    drawnow limitrate; % update process for pending callbacks
                end
                if tLeft<0
                    StopRec();
                end
            end

            function StopRec(src,event)
                obj.recFlag=false;

                UiEnable(); % enable buttons and panels
                fileName=[obj.groupName,'_',obj.cond,'_',obj.item,'_',datestr(datetime(),'dd_mm_yyyy_HH_MM_SS')]; % create file name
                obj.Tobii.SendAction('StopRec');

                obj.Audio.SaveData(fileName); % save audio data with file name
                compRecLabel.Text=[compRecLabel.Text newline fileName]; % ouput
                obj.cond='';obj.item=''; itemList.Value='None';btnNone.Value=1; % Reset conditions
                obj.calibrateFlag==false;

            end

            function resp=UiEnable()
                if obj.recFlag==true
                    btnStart.Enable='off'; btnStop.Enable='on';
                    expPanel.Enable='off';btnClose.Enable='off';
                    devicePanel.Enable='off';
                    btnConnectmotive.Enable='off';
                elseif obj.recFlag==false
                    btnStart.Enable='on'; btnStop.Enable='off';
                    expPanel.Enable='on';btnClose.Enable='on';
                    devicePanel.Enable='on';
                    btnConnectmotive.Enable='on';
                end
                resp=true;
            end

            function UiCalibration(calibOk)
                if calibOk(1,1)==true
                    resp1.Text='TG-11: Calibrated ✓'; resp1.FontColor=[0.4660 0.6740 0.1880];
                else
                    resp1.Text='TG-11: Not Calibrated'; resp1.FontColor='k';
                end
                if calibOk(2,1)==true
                    resp2.Text='TG-21: Calibrated ✓'; resp2.FontColor=[0.4660 0.6740 0.1880];
                else
                    resp2.Text='TG-21: Not Calibrated';resp2.FontColor='k';          
                end
                if calibOk(3,1)==true
                    resp3.Text='TG-81: Calibrated ✓'; resp3.FontColor=[0.4660 0.6740 0.1880];
                else
                    resp3.Text='TG-81: Not Calibrated';resp3.FontColor='k';
                end
            end

            function UpdatetLeft(recLenField,tLeftLabel)
                tLeftLabel.Text=string(recLenField.Value);
            end

            function UpdateGroupName(newName)
                obj.groupName=newName;
                % this below is to force user to recalibrate each new group
                resp1.Text='TG-11: Not Calibrated'; resp1.FontColor='k';
                resp2.Text='TG-21: Not Calibrated';resp2.FontColor='k';
                resp3.Text='TG-81: Not Calibrated';resp3.FontColor='k';
            end

            function UpdateCondSelection(source,event)
                obj.cond=event.NewValue.Text;
                % assign corresponding gain to condition
                if(obj.cond=='Quiet')
                    obj.Audio.gain=obj.gainQuiet;
                elseif (obj.cond=='Noisy')
                    obj.Audio.gain=obj.gainNoisy;
                end
            end

            function UpdateItem(newItem)
                obj.item=newItem;
            end
        end
    end
end