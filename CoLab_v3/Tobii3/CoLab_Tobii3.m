classdef CoLab_Tobii3<handle
    
    properties (Access=public)
        rusn=["TG03B-080202045791";"TG03B-080201122421";"TG03B-080201025481"]; % Recording Units Serial Numbers or IPs
        %"TG03B-080201129411"
    end
    
    methods
        function succes=SendAction(obj,action)
            succes=[];
            import matlab.net.http.*;
            
            switch action % select the addres and mssg body for specific action
                case 'Calibrate'
                    data='[]';
                    actionAddress='calibrate!run';
                case 'SetClock'
                    % Not necessary if you are not using the RU clock as reference
                    % 1) Disable NTP 2) Set date and time
                case 'StartRec'
                    data='[]';
                    actionAddress='recorder!start';                    
                case 'StopRec'
                    data='[]';
                    actionAddress='recorder!stop';
            end           
            request = RequestMessage('POST',[],data); % write request mssg
            
            disp('Waiting for response...');
            for i=1:length(obj.rusn)
                fullAddress=UriWriter(obj.rusn(i,1),actionAddress); % write URI fro address
                try
                response=send(request,fullAddress); % send request mssg with specific URI
                succes(i,1)=response.Body.Data % body data for each reponse
                % the data is a boolean that defines if the action was executd succesfully   
                catch
                    succes(i,1)=0;
                    disp(["Problem when communicating with URL for " obj.rusn(i,1)])
                end
            end
            disp('All action request sent');
            
            function uri=UriWriter(ip,address)
                import matlab.net.*;
                ipChar=convertStringsToChars(ip);
                destination=strcat('http://',ipChar,'/rest/',address);
                uri=URI(destination);
            end
        end
    end
end
