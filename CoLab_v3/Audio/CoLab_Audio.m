classdef CoLab_Audio<handle
    
    properties (Access=public)
        devName='RedNet PCIe'; % device name
        Fs=48000; % sampling freq
        bufSize=2048; % buffer size
        idx=0, % index for buffer shifting
        nC=9; % num of channels
        nSources=8; % num of sources
        gain=0; % gain multiplier
        playRec=[]; % audio player and recorder device
        fileReader={}; % file reader device
        buffer=[]; % buffer array
        audioData=[]; % data array
        underruns=0;
        overruns=0;


        audioDataCalibration =[];
        calibrateStimulus = []; %Stimulus used for temporal alignment
        stimGain=1e-2; %gain for calibration stimulus
        calIdx=0; %index for buffer of calibration
        stimDur=3; %sweep duration
        fo = 100; % Start at 109 Hz,
        f1 = 16000;  % go up to 16000 Hz

        maxSound = 0.26; %maximum value in sound
    end
    
    methods
        
        function CreateDevices(obj)
            % Create audio device Player and Recorder
            obj.playRec = audioPlayerRecorder('SampleRate', obj.Fs,'Device',...
                obj.devName,'BufferSize',obj.bufSize, 'RecorderChannelMapping', [1:6, 9:11],...
                'PlayerChannelMapping',[1:16]);%
        
            % create audio sources
            for i = 1:obj.nSources
                obj.fileReader{i} = dsp.AudioFileReader([...
                    'C:/demos/cocktailParty/Stories/T0' num2str(i) '_S0' num2str(i) '.wav'], 'SamplesPerFrame',...
                    obj.bufSize,'PlayCount',200);
            end
            obj.buffer = zeros(obj.bufSize,16); % prelocate memory

            %create and patch sweep
            t = 0:1/obj.Fs:obj.stimDur;% stimDur s at Fs sample rate
            obj.calibrateStimulus = [zeros(1,obj.bufSize),... %one frame before chirp
                chirp(t,obj.fo,obj.stimDur,obj.f1,'logarithmic'),...%chirp
                zeros(1,obj.bufSize-mod(length(t),obj.bufSize)),... %pad chirp to fit buffer
                zeros(1,obj.Fs-mod(obj.Fs,obj.bufSize))];%one second extra
        end
        
        function PlaybackAndRec(obj)
          
          
            for n = 1:obj.nSources 
                obj.buffer(:,2*n-1) = obj.fileReader{n}(); %copy samples to buffer
            end
            
           % disp(obj.gain);
            assert(abs(obj.gain)<1.6); % throw error if gain < x 1.6
            obj.buffer = obj.gain*obj.buffer; % amplify buffer
            nStart=1+obj.bufSize*obj.idx; % define start sample
            nEnd=obj.bufSize+obj.bufSize*obj.idx; % define end sample

            [obj.audioData(nStart:nEnd,:),nUnderruns,nOverruns]  = obj.playRec(obj.buffer); % play audio and record data
            obj.idx=obj.idx+1; % update index

            %check for left out samples
            if nUnderruns > 0
                fprintf('Audio player queue was underrun by %d samples.\n',nUnderruns);
                if obj.idx > 1

                    obj.underruns=obj.underruns+nUnderruns;
                end
            end
            if nOverruns > 0
                fprintf('Audio recorder queue was overrun by %d samples.\n',nOverruns);
                if obj.idx > 1
                    obj.overruns=obj.overruns+nOverruns;
                end
            end
        end
        
        function PlaybackAndRecCalibration(obj,source)
            if mod(length(obj.calibrateStimulus), obj.bufSize)~=0 %pad calibration stimulus to fit buffer size if necessary
                obj.calibrateStimulus= [obj.calibrateStimulus,...
                    zeros(1,obj.bufSize-mod(length(obj.calibrateStimulus),obj.bufSize))];
            end

            stimBuffer = zeros(length(obj.calibrateStimulus),16); % prelocate memory
            stimBuffer(:,source) = obj.stimGain*obj.calibrateStimulus; %add gain
            
            assert(max(stimBuffer(:))<obj.maxSound); % throw error if sound is too loud

            %play calibration stimulus and record calibration data
            for i=0: length(obj.calibrateStimulus)/obj.bufSize-1
                nStart=1+obj.bufSize*obj.calIdx; % define start sample
                nEnd=obj.bufSize+obj.bufSize*obj.calIdx; % define end sample
                obj.calIdx=obj.calIdx+1; %update Index
                obj.audioDataCalibration(nStart:nEnd,:) = obj.playRec(stimBuffer(1+obj.bufSize*i:obj.bufSize+obj.bufSize*i,:)); % play audio and record data
            end
       

        end


        function SaveData(obj,name)
            save(name,'obj');
            disp('Audio Data Saved');
            obj.audioData= [];
            obj.idx = 0;
            obj.audioDataCalibration=[];
            obj.calIdx=0;
            obj.overruns=0;
            obj.underruns=0;
        end
        
        function ReleaseDevices(obj)
            obj.playRec.release;
            disp('Audio device released');            
        end
    end
end
