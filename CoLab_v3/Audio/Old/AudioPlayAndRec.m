function AudioPlayAndRec(gain,cond_string)
devName='RedNet PCIe';
Fs=48000; %sampling freq
buffer=8*2048; %2048; %buffer size
nC=9; %num of channels
idx=0;
%create audioDeviceReader object
playRec = audioPlayerRecorder('SampleRate', Fs,'Device',...
    devName,'BufferSize',buffer, 'RecorderChannelMapping', [1:6, 9:11],...
    'PlayerChannelMapping', [1:16]);

nSources= 8;
for i = 1:nSources
fileReader{i} = dsp.AudioFileReader(['C:/demos/cocktailParty/Stories/T0' num2str(i) '_S0' num2str(i) '.wav'], 'SamplesPerFrame',buffer,'PlayCount',20);
end

audioToDevice = zeros(buffer,16);

%% REC
disp('Rec Start');
tic;

while rec==true
    pause(0);
    for n = 1:nSources
        audioToDevice(:,2*n-1) = fileReader{n}();
    end

    assert(abs(gain)<1.6)
    audioToDevice = gain*audioToDevice;

    nStart=1+buffer*idx;
    nEnd=buffer+buffer*idx;
    %audioData(nStart:nEnd,:)=deviceReader();
    [audioData(nStart:nEnd,:),numUnderrun,numOverrun] = playRec(audioToDevice);
    disp(['numUnderrun is ' num2str(numUnderrun)])
    disp(['numOverrun is ' num2str(numOverrun)])
    idx=idx+1;
    disp(toc);
end
delete(fig);
disp('recording completed, saving data...');

%% Save Data, release objects
save(cond_string,'audioData');
disp('Audio data saved');
playRec.release;
disp('Audio objects released');
%% Stop Button
    function buttonCallback(src,event)
        rec=false;
    end
end