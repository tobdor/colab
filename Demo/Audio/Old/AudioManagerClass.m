classdef AudioManagerClass<handle
    
    properties (Access=public)
        devName='RedNet PCIe'; % device name
        Fs=48000; % sampling freq
        buffer=8*2048; %2048; %buffer size
        nC=9; % num of channels
        nSources=8; % num of sources
        rec=false; % recording flag
        playRec=audioPlayerRecorder;
        fileReader={};
    end
    
    methods
        function AudioPlayAndRec(obj)
            %% CREATE OBJECTS
%             % create audioDeviceReader object
%             obj.playRec = audioPlayerRecorder('SampleRate', obj.Fs,'Device',...
%                 obj.devName,'BufferSize',obj.buffer, 'RecorderChannelMapping', [1:6, 9:11],...
%                 'PlayerChannelMapping',[1:16]);%
%             % create audio sources
%             for i = 1:obj.nSources
%                 obj.fileReader{i} = dsp.AudioFileReader([...
%                     'C:/demos/cocktailParty/Stories/T0' ...
%                     num2str(i) '_S0' num2str(i) '.wav'], 'SamplesPerFrame',...
%                     obj.buffer,'PlayCount',20);
%             end
%             % prelocate memory
%             audioToDevice = zeros(obj.buffer,16);
            
            %% RECORDING LOOP
            obj.rec=true;
            idx=0; % idx for buffer
            disp('Rec Start');   
            audioData=0;
            while obj.rec==true
                pause(0);
%                 for n = 1:obj.nSources
%                     audioToDevice(:,2*n-1) = obj.fileReader{n}();
%                 end
%                 assert(abs(gain)<1.6)
%                 audioToDevice = gain*audioToDevice;
%                 nStart=1+obj.buffer*idx;
%                 nEnd=obj.buffer+obj.buffer*idx;
%                 %audioData(nStart:nEnd,:)=deviceReader();
%                 [audioData(nStart:nEnd,:)] = obj.playRec(audioToDevice);
%                 idx=idx+1;   
            audioData=audioData+1;
            end            
        end
        function AudioStop(obj)
            obj.rec=false;
        end
        function AudioSave(obj,takeName)
            disp('recording completed, saving data...');            
            save(takeName,'audioData');
            disp('Audio data saved');
%             obj.playRec.release;
            disp('Audio object released');
        end
    end
end
