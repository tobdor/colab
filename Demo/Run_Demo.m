% To run a simple Demo in Colab.
% This starts a simple GUI which only allows playback of background noise,
% no recordings / device connection is possible

clear all;
clc;
addpath("Audio\")
CoLab=CoLab_Control;