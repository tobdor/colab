classdef CoLab_Control<handle

    properties (Access=public)
        %% Classes Instances
        Audio=CoLab_Audio;
        tobii_rusn = [...
            "TG03B-080202045791";"TG03B-080201122421";"TG03B-080201025481";"TG02B-080108002631";...
            "TG03B-080201140791";"TG03B-080201047861";"TG03B-080202120171";"TG02B-080108002351"]
        tobii_short = [...
            "TG3-91";"TG3-21";"TG3-81";"TG2-31";...
            "ERH3-4";"ERH3-5";"ERH3-6";"ERH2-1"]
        tobii_table  %Table to save info on Tobiis
        Tobiis % Tobii controller classes

        Motive=[]; % Added in runtime
        %% Global Variables
        project = 'Demo'
        groupNumber='demo'; % group id
        cond='Noisy'; % condition, i.e. noisy or quiet
        dBlevel
        dbA_gain1 = 68.7; %measured dBA if gain is set to 1

        % defined gain multiplier for each condition
        %gainQuiet = 10^((55-68.7)/20);
        gain=0; % gain multiplier, to be sent to audio player
        dBNoisy = 55;
        %gainNoisy= 10^((65-68.7)/20);
        recLen = 5*60;
        %% States
        recFlag=false; % recording bool for the whole apparatus
        calibrateFlag=false; % calibrated bool for Tobii glasses
    end

    methods
        function obj = CoLab_Control()

            % audio settings
            obj.Audio.CreateDevices();
            obj.Audio.gain  = 10^((obj.dBNoisy-obj.dbA_gain1)/20);

            % create tobii controllers
            n_tobiis = length(obj.tobii_rusn);
            obj.Tobiis = cell(1,n_tobiis);
            for t = 1:n_tobiis

                if strcmp(extract(obj.tobii_rusn(t),4),"3")
                    %if the rusn corresponds to TG3
                    obj.Tobiis{t} = CoLab_Tobii3(obj.tobii_rusn(t),obj.project);
                elseif strcmp(extract(obj.tobii_rusn(t),4),"2")
                    %if the rusn corresponds to TG2
                    obj.Tobiis{t} = CoLab_Tobii2(obj.tobii_rusn(t),obj.project);
                else
                    error("Problem with the Tobii rusn")
                end

            end
            obj.tobii_table = table(obj.tobii_short,obj.tobii_rusn,false(n_tobiis,1), ...
                zeros(n_tobiis,1), false(n_tobiis,1),false(n_tobiis,1),cell(n_tobiis,1),...
                'VariableNames',["TG","RUSN","Connected","Battery","Calibrated","Recording","Updated"]);

            obj.CreateUI;

        end

        function CreateUI(obj)


            %% PANELS
            % Main Window (Figure)
            fig = uifigure('Position',[300 100 1200 900]);
            fig.Name='CoLab Controller';
            uiTitle=uilabel(fig,'Text','CoLab Demo',...
                'Position',[500 800 400 100],'FontSize',28);

            % Experiment Manager
            expPanel=uipanel(fig,'Title','Experiment Manager','FontSize',20,...
                'BackgroundColor','white','Position',[100 400 295 400]);

            groupNameTitle=uilabel(expPanel,'Text','Group Number:',...
                'Position',[50 300 200 50],'FontSize',18);
            groupNameField=uieditfield(expPanel,'Value',obj.groupNumber,'Position',[50 250 200 50],'FontSize',18,...
                'ValueChangedFcn',@(groupNameField,event) UpdateGroupName...
                (groupNameField.Value));

            condTitle=uilabel(expPanel,'Text','Noise:',...
                'Position',[50 200 80 50],'FontSize',18);
            condGroup = uidropdown(expPanel,'Position',[50 150 80 50],...
                'Items',["Noisy","None","Other"],...
                'ValueChangedFcn',@UpdateCondSelection);

            dBTitle=uilabel(expPanel,'Text','approx. dB:',...
                'Position',[150 200 100 50],'FontSize',18);
            dBValue = uieditfield(expPanel,'numeric','Value',68.7+20*log10(obj.Audio.gain),'Position',[150 150 100 50],'FontSize',18,...
                'ValueChangedFcn',@(gainValueField,event) UpdatedB...
                (gainValueField.Value));

            timeTile=uilabel(expPanel,'Text','max. playback time (s):',...
                'Position',[50 100 200 50],'FontSize',18);
            timeValue = uieditfield(expPanel,'numeric','Value',obj.recLen,'Position',[50 50 200 50],'FontSize',18,...
                'ValueChangedFcn',@(timeField,event) UpdateRecLen...
                (timeField.Value));


            % Devices
            devicePanel=uipanel(fig,'Title','Device Manager','FontSize',20,...
                'BackgroundColor','white','Position',[405 400 695 400],'Enable','off');

            btnCheckConnection=uibutton(devicePanel,'ButtonPushedFcn',@CheckConnect,'Position',...
                [50 280 200 50],'Text','Check Tobii Connection','FontSize',18);

            btnCalibrate=uibutton(devicePanel,'ButtonPushedFcn',@CalibrateTobii,'Position',...
                [300 280 150 50],'Text','Calibrate Tobii','FontSize',18);

            tobiiTable = uitable(devicePanel,'Position',[50 50 600 200],'FontSize',12,"Data",obj.tobii_table);
            % check Tobii Connection
            %CheckConnect();


            % Recording
            recPanel=uipanel(fig,'Title','Background noise','FontSize',20,...
                'BackgroundColor','white','Position',[100 90 295 300]);
            btnStart=uibutton(recPanel,'ButtonPushedFcn',@StartRec,'Position',...
                [50 190 200 50],'Text','START','FontSize',18);
            btnStop=uibutton(recPanel,'ButtonPushedFcn',@StopRec,'Position',...
                [50 120 200 50],'Text','STOP','FontSize',18);
            btnClose = uibutton(recPanel,'ButtonPushedFcn',@CloseUI,'Position',...
                [50 50 200 50],'Text','Close','FontSize',18);

            % Output
            outPanel=uipanel(fig,'BackgroundColor','white','Position',...
                [405 90 695 300],'Title','Output','FontSize',20);

            tLeftPanel=uipanel(outPanel,'BackgroundColor','white',...
                'Position',[50 160 100 80],'Title','Time left','FontSize',20);
            tLeftLabel=uilabel(tLeftPanel,'Position',[10 10 150 25],...
                'FontSize',18,'Text',sprintf("%d:%d",floor(obj.recLen/60),round(mod(obj.recLen,60))));

            tTotalPanel=uipanel(outPanel,'BackgroundColor','white',...
                'Position',[50 50 100 80],'Title','Time recorded','FontSize',20);
            tTotalLabel=uilabel(tTotalPanel,'Position',[10 10 150 25],...
                'FontSize',18,'Text','0');

            compRecPanel=uipanel(outPanel,'BackgroundColor','white',...
                'Position',[170 50 450 190],'Title','Completed Recordings','FontSize',20);
            compRecLabel=uilabel(compRecPanel,'Position',[30 0 450 150],...
                'FontSize',16);
            compRecLabel.Text='';

            %% CALLBACK FUNCTIONS
            function CloseUI(src,event)
                obj.Audio.ReleaseDevices();
                closereq(); % close UI
            end

            %             function ConnectMotive(src,event)
            %                 if obj.connectFlag==false
            %                     Motive=natnet(); % instantiate class
            %                     disp('Connecting to Motive...');
            %                     Motive.connect; % connect function
            %                     disp('Connection with Motive completed');
            %                     obj.connectFlag=true;
            %                     resp4.Text='Motive connected ✓'; resp4.FontColor=[0.4660 0.6740 0.1880];
            %                     btnConnectmotive.Enable='off';
            %                 end
            %             end

            function CheckConnect(src,event)
                fprintf('Checking connection of Tobii glasses...\n')
                for t = 1:length(obj.Tobiis)
                    battery = obj.Tobiis{t}.CheckBattery();
                    obj.tobii_table.Battery(t) = battery;
                    obj.tobii_table.Connected(t) = battery>0;
                    obj.tobii_table.Updated{t} = datestr(datetime(),'HH:MM:SS dd-mm-yyyy');
                end
                UpdateTable();
            end

            function CheckBattery(src,event)
                fprintf('Checking battery of Tobii glasses...\n')
                for t = 1:length(obj.Tobiis)
                    if obj.tobii_table.Connected(t)
                        battery = obj.Tobiis{t}.CheckBattery();
                        obj.tobii_table.Battery(t) = battery;
                        obj.tobii_table.Connected(t) = battery>0;
                        obj.tobii_table.Updated{t} = datestr(datetime(),'HH:MM:SS dd-mm-yyyy');
                    end
                end
                UpdateTable();
            end

            function CalibrateTobii(src,event)
                %check connection if last update was more than 1 min ago
                if minutes(datetime()-datetime(obj.tobii_table.Updated{1},'InputFormat','HH:mm:ss dd-MM-yyyy'))>1
                    CheckConnect(src,event)
                end
                if any(obj.tobii_table.Connected)
                    fprintf('Calibrating connected Tobii glasses...\n')
                    for t = 1:length(obj.Tobiis)
                        if obj.tobii_table.Connected(t) && not(obj.tobii_table.Calibrated(t))
                            calibstate = obj.Tobiis{t}.Calibrate();
                            obj.tobii_table.Calibrated(t) = calibstate;
                        end
                    end
                    UpdateTable();
                else
                    fprintf('No Tobii glasses connected!...\n')
                end
            end

            function StartTobii(src,event)
                fprintf('Starting recording on connected Tobii glasses...\n')
                for t = 1:length(obj.Tobiis)
                    if obj.tobii_table.Connected(t)
                        record_state = obj.Tobiis{t}.Record();
                        obj.tobii_table.Recording(t) = record_state;
                    end
                end
                UpdateTable();
            end

            function StartRec(src,event)


                CheckBattery(src,event);
                tobii_active = sum(obj.tobii_table.Connected);
                tobii_battery = sum(obj.tobii_table.Battery>50);
                tobii_calib = sum(obj.tobii_table.Calibrated);
                %% Warning messages

                if tobii_calib<tobii_active
                    message = sprintf("%d out of %d Tobii glasses are not calibrated. Continue?",tobii_active-tobii_calib,tobii_active);
                    selection=uiconfirm(fig,message,'OBS','DefaultOption',2,'Icon','warning');
                    if strcmp(selection,'Cancel')
                        return
                    end
                end
                if tobii_battery<tobii_active
                    message = sprintf("%d out of %d Tobii glasses have less than 50% battery. Continue?",tobii_active-tobii_battery,tobii_active);
                    selection=uiconfirm(fig,message,'OBS','DefaultOption',2,'Icon','warning');
                    if strcmp(selection,'Cancel')
                        return
                    end
                end
                message = sprintf("Start with %d Tobii glasses and Noise at %f dB?",tobii_active,68.7+20*log10(obj.Audio.gain));
                selection=uiconfirm(fig,message,'OBS','DefaultOption',2,'Icon','warning');
                if strcmp(selection,'Cancel')
                    return
                end

                %% Recording
                obj.recFlag=true;
                UiEnable(); % disable buttons and panels


                %start tobii

%                 try
%                     StartTobii(src,event)
%                 catch
%                     uialert(fig,'Tobii could not be started!','OBS')
%                     obj.recFlag=false;
%                     UiEnable(); % enable buttons and panels
%                     return
%                 end

                halfTime=obj.recLen/2; % rec length / 2
                reminder=false; % flag for half-time reminder
                tLeft=obj.recLen; % time left


                %preallocate memory for audio data
                obj.Audio.audioData=nan(obj.Audio.Fs*tLeft,9);

                %disp('Started Recording')
                startTime=tic;
                while  obj.recFlag==true && tLeft>=0
                    tLeft=obj.recLen-toc(startTime); % update rec time left
                    tLeftLabel.Text= sprintf("%d:%d",floor(tLeft/60),round(mod(tLeft,60))); % write time left
                    tTotalLabel.Text= sprintf("%d:%d",floor(toc(startTime)/60),round(mod(toc(startTime),60))); % write time left
                    if(tLeft<halfTime && reminder==false)
                        uialert(fig,'Half time!',...
                            'OBS','Icon','info');
                        reminder=true;
                    end
                    obj.Audio.PlaybackAndRec();
                    drawnow limitrate; % update process for pending callbacks
                end
                if tLeft<0
                    StopRec();
                end
            end

            function StopRec(src,event)
                obj.recFlag=false;

                %fprintf('Stopping recording on connected Tobii glasses...\n')
                for t = 1:length(obj.Tobiis)
                    if obj.tobii_table.Connected(t)
                        stop_state = obj.Tobiis{t}.Stop();
                        obj.tobii_table.Recording(t) = not(stop_state);
                    end
                end
                UpdateTable();
                obj.Audio.ResetPlayer();

                UiEnable(); % enable buttons and panels
                tLeftLabel.Text= sprintf("%d:%d",floor(obj.recLen/60),round(mod(obj.recLen,60))); % write time left
                tTotalLabel.Text= '0'; % write time left

            end

            function UpdateTable()
                tobiiTable.Data = obj.tobii_table;

                % colorcode table

                % base color = green (everything okay)
                style = uistyle("BackgroundColor",[0 1 0]);
                addStyle(tobiiTable,style,"table","");
                % not calibrated = yellow
                style = uistyle("BackgroundColor",[1 1 0]);
                addStyle(tobiiTable,style,"row",find(not(tobiiTable.Data.Calibrated)));
                % battery < 50 = orange
                style = uistyle("BackgroundColor",[1 0.5 0]);
                addStyle(tobiiTable,style,"row",find((tobiiTable.Data.Battery)<50));
                % battery < 20 = red
                style = uistyle("BackgroundColor",[1 0 0]);
                addStyle(tobiiTable,style,"row",find((tobiiTable.Data.Battery)<20));
                % disconnected / unused glasses = grey
                style = uistyle("BackgroundColor",[0.8 0.8 0.8]);
                addStyle(tobiiTable,style,"row",find(not(tobiiTable.Data.Connected)));


            end

            function resp=UiEnable()
                if obj.recFlag==true
                    btnStart.Enable='off'; %btnStop.Enable='on';
                    expPanel.Enable='off';btnClose.Enable='off';
                    %devicePanel.Enable='off';
                elseif obj.recFlag==false
                    btnStart.Enable='on'; %btnStop.Enable='off';
                    expPanel.Enable='on';btnClose.Enable='on';
                    %devicePanel.Enable='on';
                end
                resp=true;
            end

            function UpdateGroupName(newNumber)
                obj.groupNumber=newNumber;
                % this below is to force user to recalibrate each new group
                obj.tobii_table.Calibrated(:) = false;
                UpdateTable;
            end

            function UpdateCondSelection(source,event)
                obj.cond=event.Value;
                % assign corresponding gain to condition
                if strcmp(obj.cond,'Noisy')
                    dBValue.Value = obj.dBNoisy;
                    obj.Audio.gain=10^((obj.dBNoisy-obj.dbA_gain1)/20);
                elseif strcmp(obj.cond,'None')
                    obj.Audio.gain=0;
                    dBValue.Value = -Inf;
                end
                obj.dBlevel = dBValue.Value;
            end

            function UpdatedB(NewdB)
                obj.Audio.gain = 10^((NewdB-obj.dbA_gain1)/20);
                if NewdB == -Inf
                    condGroup.Value = condGroup.Items(1);
                elseif NewdB == obj.dBNoisy
                    condGroup.Value = condGroup.Items(3);
                else
                    condGroup.Value = condGroup.Items(4);
                end
                obj.dBlevel = NewdB;
            end

            function UpdateRecLen(NewRecLen)
                obj.recLen = NewRecLen;
                tLeftLabel.Text = sprintf("%d:%d",floor(obj.recLen/60),round(mod(obj.recLen,60)));
            end

        end
    end
end