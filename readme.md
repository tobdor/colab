# Code for running experiments in the Conversation Lab

## CoLab_v3
Working code to play background noise and record via microphones and Tobii glasses 3. 
 - Requires 3 Tobii glasses 3
 - from Summer 2023

## Demo
Simplified version of CoLab_v3 / AViCCI code for demonstrating background noise. Opens GUI that can only play background noise. No Tobii glasses needed, no recording possible